export class Cita
{
    estado:string
    ci:string
    nombre:string
    doctor:string
    dia:number
    hora:number
    modulo:string
    recetario:string

    constructor()
    {
        this.ci='';
    }
}
export class Doctor
{
    codigo:string
    nombre:string
}
export class Seccion
{
    codigo:string
    nombre:string
}