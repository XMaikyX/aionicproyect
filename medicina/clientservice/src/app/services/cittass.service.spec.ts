import { TestBed } from '@angular/core/testing';

import { CittassService } from './cittass.service';

describe('CittassService', () => {
  let service: CittassService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CittassService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
