import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DoctoresService {

  apiURL='https://basedatosproyecto-77223.firebaseio.com/Doctores'

  constructor(private doctoresServicio:HttpClient) { }

  public getDoctor(codigo='')
  {
    if (codigo=='')
    return this.doctoresServicio.get(`${this.apiURL}.json`).toPromise()
    return this.doctoresServicio.get(`${this.apiURL}/${codigo}.json`).toPromise()
  }

}
