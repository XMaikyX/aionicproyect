import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ModulosService {

  apiURL='https://basedatosproyecto-77223.firebaseio.com/Modulo'

  constructor(private moduloServicio:HttpClient) { }

  public getModulo(modulo='')
  {
    if (modulo=='')
    return this.moduloServicio.get(`${this.apiURL}.json`).toPromise()
    return this.moduloServicio.get(`${this.apiURL}/${modulo}.json`).toPromise()
  }
}
