import { Component, OnInit } from '@angular/core';
import { Cita,Doctor,Seccion} from 'src/app/folder/interfaces/cita';
import { ICrud } from 'src/app/folder/interfaces/ICrud';
import { CittassService } from 'src/app/services/cittass.service';
import { DoctoresService } from 'src/app/services/doctores.service';
import { ModulosService } from 'src/app/services/modulos.service';
import { ToastController } from '@ionic/angular';



@Component({
  selector: 'app-cita',
  templateUrl: './cita.page.html',
  styleUrls: ['./cita.page.scss'],
})
export class CitaPage implements OnInit, ICrud {
  //CITA
  public citaMedica: Cita = {estado:'En espera',ci:'000',nombre:'vacio', doctor:'Vacio', dia:0,hora:0, modulo:'vacio',recetario:"nada asignado"}
  public citasMedica: Cita[]=[];
  //DOCTOR
  public doctorMedic: Doctor[]=[];
  
  //SECCION
  public seccionsMedic: Seccion[]=[];

  public moduloMedico: {cod:string,seccion:string}[]=
  [
    {cod:'mg', seccion:'Medicina General'},
    {cod:'od', seccion:'Odontologia'},
    {cod:'pe', seccion:'Pediatria'},
    {cod:'gi', seccion:'Ginecologia'},
    {cod:'fi', seccion:'Fisioterapia'}
  ];

  constructor(private cliente:CittassService, private doctor:DoctoresService,private seccion:ModulosService, private toast:ToastController) { 
    
  }
  
  async mostrarMensaje(notificacion:string, duracion:number){
    const notificacions=await this.toast.create({message:notificacion, duration:duracion})

    notificacions.present();
  }

  guardar(): void {
    this.cliente.postCita(this.citaMedica).then(resp=>{
      
      this.mostrarMensaje('Cita Guardada Correctamente',2000)
      //console.log('se grabo su cita')
    })
    .catch(error=>
      this.mostrarMensaje('No se pudo Guardar',200)
      )
  }

  parametros(cedu:string)
  {
    this.cliente.getCita(cedu).then(resp=>{
      this.citaMedica=<Cita>resp;
    })
    .catch(error=>{
      console.log(error )
    })
  }

  consultar(): void {
    this.cliente.getCita().then(respuesta=>{
      
      this.citasMedica=[];
      for(let elemento in respuesta)
      {
        this.citasMedica.push(respuesta[elemento]);
      }
      
    })
    .catch(error=>{
      console.log(error)
    })
  }
  eliminar(): void {
    this.cliente.deleteCita(this.citaMedica.ci).then(resp=>{
      console.log('se elimino correctamente')  
    })
    .catch(error=>{
      console.log('no se pudo eliminar')
    })
  }

  ngOnInit() {
  }

  nuevo()
  {
    this.citaMedica={estado:'En Espera',ci:'',nombre:'',dia:0,hora:0,doctor:'',modulo:'',recetario:'nada asignado'};
    this.doctor.getDoctor().then(respuesta=>{
      this.doctorMedic=[];
      for(let elemento in respuesta)
      {
        this.doctorMedic.push(respuesta[elemento]);
      }
      
    })
    this.seccion.getModulo().then(respuesta=>{
      this.seccionsMedic=[];
      for(let elemento in respuesta)
      {
        this.seccionsMedic.push(respuesta[elemento]);
      }
      
    })
  }
}